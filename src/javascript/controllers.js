var medsLister = angular.module('medsLister',[]);

medsLister.controller('ListNames',function($scope,$http){
  $http.get('../javascript/fulldata.json').success(function(data){
    $scope.medicaments = data;     
  });
});


medsLister.controller('ListDetails',function($scope,$http,$routeParams){
  $http.get('../javascript/fulldata.json')
  .success(function(data){
    $scope.medicaments = data;
    $scope.whichItem =  $routeParams.itemId; });  
});