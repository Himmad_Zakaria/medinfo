var medinfo = angular.module('medinfo',[
  'ngRoute',
  'medsLister'
]);
medinfo.config(['$routeProvider',function($routeProvider){
  $routeProvider.
  when('/list',{
    templateUrl: 'partials/list.html',
    contorller: 'medsLister'
  }).
  when('/details/:itemId',{
    templateUrl: 'partials/details.html',
    controller: 'ListDetails'
  }).
  otherwise({
      redirectTo: '/list'
  });
}]);